import telebot
import pymongo
import time
import requests
import os
from collections import defaultdict
from telebot import types
from math import sin, cos, sqrt, atan2, radians


bot_token = os.getenv('BOT_TOKEN')
maps_token = os.getenv('MAPS_TOKEN')
db_password = os.getenv('DB_PASSWD')

geocoding_url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng={},{}&key={}{}'
db_url = 'mongodb+srv://telebot:{}@cluster0-hjxox.mongodb.net/test?retryWrites=true&ssl=true'.format(db_password)

client = pymongo.MongoClient(db_url, ssl=True)
db = client.locations
locs = db.locations

START, NAME, PHOTO, LOCATION, CONFIRMATION = range(5)
USER_STATE = defaultdict(lambda: START)

bot = telebot.TeleBot(bot_token)

locations = defaultdict(lambda: {})

@bot.message_handler(commands=['start'])
def handle_message(message):
    text = '''/add - Начать добавление нового места
/stop - Прервать добавление нового места и удалить уже полученные данные
/list - Показать последние десять сохраненных локаций
/reset - Удалить все сохраненные локации'''
    bot.send_message(chat_id=message.chat.id, text=text)

@bot.message_handler(commands=['reset'])
def handle_reset(message):
    user_id = message.chat.id
    db.drop_collection(str(user_id))
    bot.send_message(user_id, text='Все сохраненные локации удалены')
    update_state(user_id, START)
    
@bot.message_handler(commands=['stop'])
def handle_stop(message):
    user_id = message.chat.id
    update_state(message, START)
    locations.pop(user_id, None)
    bot.send_message(user_id, text='Ввод сброшен! /add, чтобы начать снова')
    
@bot.message_handler(commands=['list'])
def handle_list(message):
    user_id = message.chat.id
    user_locations = list(db[str(user_id)].find()[:10])
    if len(user_locations) == 0:
        bot.send_message(user_id, text='Пока нет сохраненных локаций! /add, чтобы добавить новую.')
    for location in user_locations:
        loc_name = location['name']
        loc_address = get_address_by_latlong(*location['coords'])
        bot.send_photo(user_id, photo=location['photo'],
                       caption='{}, {}'.format(loc_name, loc_address))

@bot.message_handler(commands=['add'], 
                     func=lambda message: get_state(message) == START)
def handle_message(message):
    user_id = message.chat.id
    bot.send_message(chat_id=user_id, text='Как называется место?')
    update_state(message, NAME)
    
@bot.message_handler(func=lambda message: get_state(message) == NAME)
def handle_name(message):
    user_id = message.chat.id
    update_location(user_id, 'name', message.text)
    bot.send_message(chat_id=user_id, text='Пожалуйста, прикрепите фото')
    update_state(message, PHOTO)
    
@bot.message_handler(content_types=['photo'], 
                     func=lambda message: get_state(message) == PHOTO)
def handle_photo(message):
    user_id = message.chat.id
    update_location(user_id, 'photo', message.photo[2].file_id)
    bot.send_message(chat_id=user_id, text='Пожалуйста, отправьте свое текущее местоположение')
    update_state(message, LOCATION)
    
@bot.message_handler(content_types=['location'], 
                     func=lambda message: get_state(message) == LOCATION)
def handle_loc(message):
    user_id = message.chat.id
    update_location(user_id, 'coords', [message.location.latitude, 
                                                message.location.longitude])
                                                
    location = get_location(user_id)
    loc_name = location['name']
    loc_photo = location['photo']
    loc_address = get_address_by_latlong(*location['coords'])
    
    reply_kb = get_confirm_kb()
    
    bot.send_photo(chat_id=user_id, 
                   caption='{}, {}'.format(loc_name, loc_address), 
                   photo=loc_photo,
                   reply_markup=reply_kb)
    update_state(message, START)
    
@bot.message_handler(content_types=['location'], 
                     func=lambda message: get_state(message) != LOCATION)
def handle_closest_locs(message):
    user_id = message.chat.id
    user_loc = [message.location.latitude, message.location.longitude]
    user_locations = list(db[str(user_id)].find())
    if len(user_locations) == 0:
        bot.send_message(user_id, text='Пока нет сохраненных локаций! /add, чтобы добавить новую.')
    for location in user_locations:
        distance = get_distance(user_loc, location['coords'])
        if distance <= 500:
            loc_name = location['name']
            loc_address = get_address_by_latlong(*location['coords'])
            bot.send_photo(user_id, photo=location['photo'],
                           caption='{}, {}'.format(loc_name, loc_address))
    
@bot.callback_query_handler(func=lambda message: message.data in ['save', 'delete'])
def handle_confirmation(message):
    user_id = message.from_user.id
    if message.data == 'save' and user_id not in locations:
        bot.send_message(user_id, text='Нет новых данных для сохранения! Используйте /add, чтобы добавить новую локацию.')
    elif message.data == 'save':
        location = get_location(user_id)
        location['created_at'] = int(time.time())
        print('location to ibsert', location)
        db[str(user_id)].insert_one(location)
        bot.send_message(user_id, text='Сохранено! Используйте /list, чтобы посмотреть последние 10 локаций')
    else:
        bot.send_message(user_id, text='Ввод удален! Используйте /list, чтобы посмотреть последние 10 локаций')
    locations.pop(user_id, None)

def get_state(message):
    return USER_STATE[message.chat.id]
    
def update_state(message, state):
    USER_STATE[message.chat.id] = state
    
def update_location(chat_id, key, value):
    locations[chat_id][key] = value

def get_location(chat_id):
    return locations[chat_id]
    
def get_address_by_latlong(lat, long):
    geo_response = requests.get(geocoding_url.format(lat, long, maps_token, ''))
    country_code = geo_response.json()['results'][1]['address_components'][-2]['short_name']
    geo_response_with_locale = requests.get(geocoding_url.format(lat, long, 
                                            maps_token, '&language={}'
                                            .format(country_code)))
    formatted_address = geo_response_with_locale.json()['results'][1]['formatted_address']
    return formatted_address
    
def get_confirm_kb():
    markup = types.InlineKeyboardMarkup(row_width=2)
    yes_btn = types.InlineKeyboardButton(text='Сохранить', callback_data='save')
    no_btn = types.InlineKeyboardButton(text='Отменить и удалить', 
                                        callback_data='delete')
    markup.add(yes_btn, no_btn)
    return markup
    
def get_distance(user_location, target_location):
    earth_radius = 6378137.0
    lat1 = radians(user_location[0])
    long1 = radians(user_location[1])
    lat2 = radians(target_location[0])
    long2 = radians(target_location[1])
    
    delta_lat = lat2 - lat1
    delta_long = long2 - long1
    a = (sin(delta_lat / 2)) ** 2 + cos(lat1) * cos(lat2) * (sin(delta_long / 2)) ** 2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    distance = earth_radius * c
    
    return int(distance)
    

bot.polling()
